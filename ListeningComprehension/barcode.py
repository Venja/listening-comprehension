#This is just a test to check if a connected scanner works

import sys
import serial

ser = serial.Serial()
ser.baudrate = 9600
PORT = 1
#for windows
ser = serial.Serial(port='/dev/ttyUSB0',
 baudrate=9600, timeout=2)
barcode = ser.read(1000000)
print ('This is the barcode scanned: ' + barcode.decode("latin-1"))
ser.close()
