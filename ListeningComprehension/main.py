#Copyright Jana Huchtkoetter, 2016
#Creative Commons Attribution-ShareAlike 4.0 International License
#You are free to use, share and adapt this sourcecode
#It is designed to be executed on a Raspbian system, with a QR code scanner connected via USB, a button with input on pin 7 and a LED output on pin 5

#Please be aware that this affects ONLY the source code
#especially the files contained in the sound folder and in predefined and it subfolders are under CC BY-NC-SA 3.0
#further licenses are

#applause.ogg
#Shortened, original Auditorium Applause Sound By Thore  [Public Domain], http://soundbible.com/1260-Auditorium-Applause.html

#buzzer.ogg
#von BlastOButter42 (Eigenes Werk) [Public domain], via Wikimedia Commons

#Warnings
#created using DSpeech by Dimitrios Coutsoumbas (http://dimio.altervista.org/eng/)

#the files contained in the sound folder and in predefined and it subfolders are under CC BY-NC-SA 3.0
#please see the LICENSE.txt for a detailed list of all audio files

from Gamecontroller import *


def main():
    contr = Gamecontroller()


if __name__ == '__main__':
    main()
