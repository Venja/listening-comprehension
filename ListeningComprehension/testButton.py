#a test to check if a connected button works on click

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

GPIO.setup(7, GPIO.IN)

GPIO.setup(5, GPIO.OUT)

counter=0
while True:
    print(GPIO.input(7))

    if(GPIO.input(7) ==1):

        GPIO.output(5, True)
    if(GPIO.input(7) ==0):

        GPIO.output(5, False)

    counter+=1

GPIO.cleanup()
