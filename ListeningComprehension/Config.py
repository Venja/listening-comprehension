#Copyright Jana Huchtkoetter, 2016
#Creative Commons Attribution-ShareAlike 4.0 International License
#You are free to use, share and adapt this sourcecode
#It is designed to be executed on a Raspbian system, with a QR code scanner connected via USB, a button with input on pin 7 and a LED output on pin 5

#Please be aware that this affects ONLY the source code
#especially the files contained in the sound folder and in predefined and it subfolders are under CC BY-NC-SA 3.0
#further licenses are

#applause.ogg
#Shortened, original Auditorium Applause Sound By Thore  [Public Domain], http://soundbible.com/1260-Auditorium-Applause.html

#buzzer.ogg
#von BlastOButter42 (Eigenes Werk) [Public domain], via Wikimedia Commons

#Warnings
#created using DSpeech by Dimitrios Coutsoumbas (http://dimio.altervista.org/eng/)

#the files contained in the sound folder and in predefined and it subfolders are under CC BY-NC-SA 3.0
#please see the LICENSE.txt for a detailed list of all audio files

from datetime import date

LOCATIONS_WORD_CSV = 'config.csv'
LOCATIONS_BASE_PATH = '/home/pi/Documents/HuchtkoetterProjekt/ListeningComprehension/'
LOCATIONS_SOUND_PATH = 'sounds/'

WARNING_UNKNOWN_WORD = 'unkownWord.ogg'
WARNING_ERROR_RANDOM = 'errorRandomWord.ogg'

LICENSE_TXT = 'LICENSE.txt'

CONFIG_FILE = 'configuration.ini'

SECTION_GAME='game_settings'
OPTION_PENSUM_SET_ON = 'set_on'
OPTION_PENSUM_TIME_SET='time_set'
OPTION_PENSUM_AMOUNT = 'pensum_amount'

#TODO MD5 hash Change Mode & print QR code
#MD5 hash of change mode
KEYWORD_CHANGE_MODE = 'Dimanche' #a test, to be changed

PREDEFINED_FRENCH_KEY = '64e12ab6aa0d4c7473286d43b6a35fc8'
PREDEFINED_FRENCH_FOLDER = '/home/pi/Documents/HuchtkoetterProjekt/ListeningComprehension/predefined/french/'
PREDEFINED_FRENCH_CSV = '/home/pi/Documents/HuchtkoetterProjekt/ListeningComprehension/predefined/french/exportedWords.csv'
PREDEFINED_SPANISH_KEY = '4757b7df2140d47d35c37e54a70ad41d'
PREDEFINED_SPANISH_FOLDER = '/home/pi/Documents/HuchtkoetterProjekt/ListeningComprehension/predefined/spanish/'
PREDEFINED_SPANISH_CSV = '/home/pi/Documents/HuchtkoetterProjekt/ListeningComprehension/predefined/spanish/exportedWords.csv'

PENSUM_KEY_WEEK = 'week'
PENSUM_KEY_DAY = 'day'
AFTER_SCAN = 'scan'
AFTER_PLAY = 'play'
AFTER_BUTTON = 'poll'
PENSUM_SET_ON = date.today()#THAT needs to be in a config!
PENSUM_TIME_SET = PENSUM_KEY_DAY #Day ist default, config as well
PENSUM = 10
PENSUM_FINISHED=0
TIMES = [(PENSUM_KEY_WEEK, "weekly"), (PENSUM_KEY_DAY, "daily")]

GAME_BACKOFF=700
