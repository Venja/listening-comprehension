#Copyright Jana Huchtkoetter, 2016
#Creative Commons Attribution-ShareAlike 4.0 International License
#You are free to use, share and adapt this sourcecode
#It is designed to be executed on a Raspbian system, with a QR code scanner connected via USB, a button with input on pin 7 and a LED output on pin 5

#Please be aware that this affects ONLY the source code
#especially the files contained in the sound folder and in predefined and it subfolders are under CC BY-NC-SA 3.0
#further licenses are

#applause.ogg
#Shortened, original Auditorium Applause Sound By Thore  [Public Domain], http://soundbible.com/1260-Auditorium-Applause.html

#buzzer.ogg
#von BlastOButter42 (Eigenes Werk) [Public domain], via Wikimedia Commons

#Warnings
#created using DSpeech by Dimitrios Coutsoumbas (http://dimio.altervista.org/eng/)

#the files contained in the sound folder and in predefined and it subfolders are under CC BY-NC-SA 3.0
#please see the LICENSE.txt for a detailed list of all audio files


from tkinter import *
from tkinter import messagebox, scrolledtext
import tkinter.ttk as ttk
from tkinter.filedialog import askopenfilename, askdirectory
import Gamecontroller #never used, but needed as controller is of type Gamecontroller
import Config

#TODO add keyboard navigation
class GameplayFrame(Frame):

    def __init__(self, parent, controller):
        ttk.Frame.__init__(self, parent)
        #parent is the ttk root, controller is a reference to the Gamecontroller
        self.parent = parent
        self.__controller = controller
        self.initUI()


    def initUI(self):
        #configure parent (ttk root) title & menu
        self.parent.columnconfigure(0,weight=1)
        self.parent.rowconfigure(0,weight=1)
        self.parent.title("Training")
        menubar = Menu(self.parent)
        #create a menu to be added as a "pull down" point to the main menu
        submenuNavigate = Menu(menubar, tearoff=0)
        submenuNavigate.add_command(label="Set Pensum", command=self.openDialogPensum)
        submenuNavigate.add_command(label="Load new vocabulary", command=self.openDialogConfig)
        submenuNavigate.add_command(label="Change Game Type", command=self.__controller.changeGameType)
        self.parent.protocol("WM_DELETE_WINDOW", self.on_exit)

        menubar.add_cascade(label="Configurate game", menu=submenuNavigate)

        menubar.add_command(label="Licenses and Credit", command=self.openDialogLicense)
        menubar.add_command(label="Quit!", command=self.on_exit)
        self.parent.config(menu=menubar)

        self.style = ttk.Style()
        self.style.theme_use("clam")
        self.style.configure('TFrame',background="white")
        self.pack(fill=BOTH, expand=1)

        #elements should take full space - one frame takes full space
        #left column takes full, aswell as second row
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=0)
        self.rowconfigure(1,weight=1)

        #tprogressBar
        self.style.configure('Horizontal.TProgressbar', background="green", anchor=CENTER, relief='raised')
        self.progress = ttk.Progressbar(self, orient='horizontal', mode='determinate')
        self.progress.grid(row=0,column=0,columnspan=2,sticky=W+E+S+N, padx=5, pady=2)
        self.progress['maximum']=Config.PENSUM

        #create word Label
        self.style.configure('word.TLabel', background="white", anchor=CENTER, font=("TkDefaultFont","24","bold"))
        self.wordLb = ttk.Label(self, text="None", style='word.TLabel')
        self.wordLb.grid(row=1,column=0,sticky=W+E+S+N,pady=2)

        #create Stop Button
        self.style.configure('stop.TButton',background="red",foreground="white",anchor=CENTER, font=("TkDefaultFont", "24","bold"))
        self.stopBt = ttk.Button(self,text="STOP!",style='stop.TButton', state=DISABLED, command=self.__controller.GUIButtonInput)
        self.stopBt.grid(row=1,column=1,sticky=W+E,ipady=1, ipadx=0,padx=2)

        #start Game
        self.startGame()
        self.__controller.startGame()#to make sure GUI is fully initiated on game start

    def gameTypeChange(self):
        self.__controller.changeGameType()
        self.startGame() # to update GUI

    def on_exit(self):
        #called from quit-Menu and x Buton
        #GameController ends the ttk root and own processes
        print("Exiting Window")
        self.__controller.endProg()

    def openDialogLicense(self):
        self.__controller.stopGame()
        try:
            afile = open(Config.LOCATIONS_BASE_PATH + Config.LICENSE_TXT, 'r')
            dialog = LicenseDialog(self, afile.read(),"Licenses and Credits")
            afile.close()
            self.wait_window(dialog)
        except Exception as e:
            messagebox.showwarning("File Problem",
            "Couldn't read file '%s': %s" % (Config.LICENSE_TXT, str(e)))
        #print("result: "+str(dialog.result))
        self.__controller.startGame() #go back to game

    def openDialogPensum(self):
        #Stop scanning while dialog open to keep thins interactive
        self.__controller.stopGame()
        try:
            dialog = PensumDialog(self, "Enter Pensum as integer: ", Config.TIMES,"Set Pensum")
            self.wait_window(dialog)
            #print("result: "+str(dialog.result))
        except Exception as e:
            print(format(e))
        else:
            self.progress['maximum'] = dialog.result
            self.resetProgress()
            self.__controller.setPensum(dialog.result,dialog.input.get())
        self.__controller.startGame() #go back to game

    def openDialogConfig(self):
        #Stop scanning while dialog open to keep thins interactive
         self.__controller.stopGame()
         try:
            dialog = LoadConfigDialog(self, "Load new words")
            self.wait_window(dialog)
            print("folder: "+str(dialog.folder))
            print("file: "+str(dialog.file))
            print("type: "+str(dialog.type.get()))
         except Exception as e:
            print(format(e))
         else:
            self.__controller.updateConfig(dialog.folder,dialog.file,dialog.type.get())
         self.__controller.startGame() #go back to game


    def startGame(self):
        if self.__controller.gameType==1:
            self.setWord("Scanning...")
        else:
            self.setWord("Waiting for \n button")
        self.parent.update() #apply the changes before really starting the read

    def stopRound(self, correct):
        self.stopBt['state']=DISABLED
        if correct:
            self.wordLb['text']="Correct!"
            print("Value: "+str(self.progress['value']))
            #each steo adds .1 to value, if value reaches maximum bar is shown empty/restarts
            if (self.progress['value']+1 < self.progress['maximum']):
                self.progress.step()
            else:
            #to keep bar looking full when maximum-> pensum is reached
                self.progress['value']=self.progress['maximum'] - 0.01 #otherwise reaching max will empty the bar
        else:
            self.wordLb['text']="False!"

    def startRound(self, word):#only gameType 1
        self.setWord(word)
        self.stopBt['state'] = NORMAL
        self.__controller.pollButton()


    def setWord(self,word):
        self.wordLb['text']=word

    def resetProgress(self):
        self.progress['value']=0

#base class for the pensum dialog
class SimpleDialog(Toplevel):
    def __init__(self, parent, textLb, title=None):
        Toplevel.__init__(self,parent)
        #transient on top of master (gameframe), control goes back when closed
        self.transient(parent)
        self.parent = parent
        Label(self, text=textLb).grid(row=0, column=0)

        self.btOk = ttk.Button(self,text="Ok", state=NORMAL, command=self.ok)
        self.btOk.grid(row=1)

        self.e1 = Entry(self)
        self.e1.grid(row=0, column=1)
        #set focus
        self.e1.focus_set()
        self.grab_set()


    def close(self):
        #give focus back to parent befor closing
        self.parent.focus_set()
        self.destroy()

    def ok(self):
        self.result=self.e1.get()
        self.close()


class PensumDialog(SimpleDialog):
    def __init__(self, parent, textLb, listOfChoices, title=None):
        super(PensumDialog, self).__init__(parent,textLb,title)
        self.btOk.grid(row=2)
        self.result=""
        self.containter = Frame(self)
        self.input = StringVar()
        self.input.set(listOfChoices[0][0]) # initialize)
        colCounter=0
        #create a set of radio buttons containing all the period options for a pensum
        for timeConst, text in listOfChoices:
            b = Radiobutton(self.containter, text=text, variable=self.input, value=timeConst, indicatoron=0)
            b.grid(sticky=W+E, row=0, column=colCounter)
            colCounter+=1
        self.containter.grid(row=1,sticky=W+E, columnspan=2)

    def ok(self):
        try:
            self.result = int(self.e1.get())
        except ValueError:
            self.e1.focus_set() #When not int, go back to e1 for new input
        else:
            print(self.input.get())
            self.close()

#Dialog to load new vocabulary
class LoadConfigDialog(Toplevel):
    def __init__(self, parent, title=None):
        super(LoadConfigDialog, self).__init__()
        self.file = ""
        self.folder = ""
        self.type =IntVar()
        self.type.set(0)
        self.transient(parent)
        self.parent = parent
        Label(self, text=".csv file:").grid(row=0, column=0)
        Label(self, text="sounds folder").grid(row=1, column=0)

        self.btOk = ttk.Button(self,text="Ok", state=NORMAL, command=self.ok)
        self.btOk.grid(row=3, column=0)

        self.container = Frame(self)
        b1 = Radiobutton(self.container, text="Overwrite", variable=self.type, value=1, indicatoron=0)
        b1.grid(sticky=W+E, row=0, column=1)
        b1 = Radiobutton(self.container, text="Append", variable=self.type, value=0, indicatoron=0)
        b1.grid(sticky=W+E, row=0, column=0)
        self.container.grid(row=2, sticky=W+E, column=0, columnspan=3)

        self.e1 = Entry(self)
        self.e1.grid(row=0, column=1)

        #button opens a dialog which puts the path of the chosen file into e1
        self.btLoadCsv = ttk.Button(self,text="Search", state=NORMAL, command=lambda: self.getFile(self.e1)) #lambda needed for the argument
        self.btLoadCsv.grid(row=0, column=2)

        self.e2 = Entry(self)
        self.e2.grid(row=1, column=1)

        #button opens a dialog which puts the path of the chosen folder into e2
        self.btLoadSounds = ttk.Button(self,text="Search", state=NORMAL, command=lambda: self.getFolder(self.e2))
        self.btLoadSounds.grid(row=1, column=2)
        #take fcus, put initial focus on e1
        self.e1.focus_set()
        self.grab_set()

#opens a file dialog & writes the result into the given entry box
    def getFile(self, entry):
        filename = askopenfilename()
        entry.delete(0,END)
        entry.insert(0,filename)
        self.file = filename

#opens a folder dialog & writes the result into the given entry box
    def getFolder(self, entry):
        foldername = askdirectory(parent=self.parent,initialdir=Config.LOCATIONS_BASE_PATH,title='Pick a directory')
        entry.delete(0,END)
        entry.insert(0,foldername)
        self.folder = foldername

    def close(self):
        self.parent.focus_set()
        print(self.type)
        self.destroy()

    def ok(self):
        self.result=self.e1.get()
        self.close()


#Dialog to show the licenses from License.txt
class LicenseDialog(Toplevel):
    def __init__(self, parent, text, title=None):
        super(LicenseDialog, self).__init__()
        self.transient(parent)
        self.parent = parent
        self.rowconfigure(0,weight=1)
        self.minsize(200,400)

        self.textField = scrolledtext.ScrolledText(self)
        self.textField.config(background='white')
        self.textField.insert(0.0, text)
        self.textField.grid(row=0, sticky=W+E+S+N)

        #take focus
        self.textField.focus_set()
        self.grab_set()