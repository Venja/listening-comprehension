#Copyright Jana Huchtkoetter, 2016
#Creative Commons Attribution-ShareAlike 4.0 International License
#You are free to use, share and adapt this sourcecode
#It is designed to be executed on a Raspbian system, with a QR code scanner connected via USB, a button with input on pin 7 and a LED output on pin 5

#Please be aware that this affects ONLY the source code
#especially the files contained in the sound folder and in predefined and it subfolders are under CC BY-NC-SA 3.0
#further licenses are

#applause.ogg
#Shortened, original Auditorium Applause Sound By Thore  [Public Domain], http://soundbible.com/1260-Auditorium-Applause.html

#buzzer.ogg
#von BlastOButter42 (Eigenes Werk) [Public domain], via Wikimedia Commons

#Warnings
#created using DSpeech by Dimitrios Coutsoumbas (http://dimio.altervista.org/eng/)

#the files contained in the sound folder and in predefined and it subfolders are under CC BY-NC-SA 3.0
#please see the LICENSE.txt for a detailed list of all audio files

import csv
import random
import datetime
import Config
import shutil
import os
import configparser



#TODO shorten samedi
#TODO shorten applause
class DataManager(object):

    def __init__(self, path):
        random.seed() #initialize the random
        self.__randTries=0
        self.__basepath = path
        self.__audioFiles = []  # for easier search, must always have same index
        self.__words = []  # indexed the same as audioFiles
        self.__configParser=configparser.ConfigParser()
        self.__readConfigFile()
        self.readCSV()  # loads the words into memory
        self.pensum=Config.PENSUM #Config.PENSUm set in readConfigFile
        self.tspPensumChange = datetime.datetime.now()

    def __readConfigFile(self):
        self.__configParser.read('configuration.ini')

        #when options arenot there the defaults written in Config will sta
        if self.__configParser.has_option('game_settings', 'set_on'):
            dateWithTime = datetime.datetime.strptime(self.__configParser['game_settings']['set_on'], "%d-%m-%Y")
            Config.PENSUM_SET_ON = dateWithTime.date()
            #print( self.__configParser.has_option('game_settings', 'set_on'))

        if self.__configParser.has_option('game_settings', 'time_set'):
            Config.PENSUM_TIME_SET = self.__configParser['game_settings']['time_set']

        if self.__configParser.has_option('game_settings', 'pensum_amount'):
            Config.PENSUM = int(self.__configParser['game_settings']['pensum_amount'])

        if self.__configParser.has_option('game_settings', 'finished_amount'):
             Config.PENSUM_FINISHED= float(self.__configParser['game_settings']['finished_amount'])

    def writeConfig(self, finished):
        #atm written on close
        self.__configParser['game_settings']={}
        self.__configParser['game_settings']['set_on'] = Config.PENSUM_SET_ON.strftime('%d-%m-%Y')
        self.__configParser['game_settings']['time_set'] = Config.PENSUM_TIME_SET
        self.__configParser['game_settings']['pensum_amount'] = str(Config.PENSUM)
        self.__configParser['game_settings']['finished_amount']= str(finished)

        with open(Config.CONFIG_FILE, 'w') as configfile:
            self.__configParser.write(configfile)
            configfile.close()

    def readCSV(self):
        #reset lists before reading
        self.__audioFiles = []
        self.__words = []
        #gets the words and audioFile  locatiosn in
        config = open(self.__basepath + Config.LOCATIONS_WORD_CSV, 'r')
        reader = csv.reader(config, delimiter=',') #comma separated
        for row in reader:
            if len(row) >= 2: #2 are needed word & file location
                self.__words.append(row[0].lower())
                self.__audioFiles.insert(len(self.__words) - 1, row[1])
        config.close()

    def appendToCSV(self, word, audio):
        fields = [word, audio]
        with open(self.__basepath + Config.LOCATIONS_WORD_CSV, 'a') as f: #a for append
            writer = csv.writer(f, delimiter=',')
            writer.writerow(fields) #writes one row
            f.close()
        self.__words.append(word)
        self.__audioFiles.insert(len(self.__words) - 1, audio)

    def replaceCSV(self, newCSV):
        shutil.copyfile(newCSV, Config.LOCATIONS_BASE_PATH+Config.LOCATIONS_WORD_CSV)
        self.readCSV()

    def extendCSV(self, newCSV):
        #processes a given csv
        config = open(newCSV, 'r')
        reader = csv.reader(config, delimiter=',')
        word=""
        audio=""
        for row in reader: #each row is appended alone
            #print(row)
            if len(row) >= 2: #and checked if long enough
                word = row[0].lower()
                audio = row[1]
                self.appendToCSV(word,audio)
        config.close()

    def getAudioFile(self, id):  # always catch LookupError
        return self.__audioFiles[id]

    def getWord(self, id):  # always catch LookupError
        print(self.__words)
        return self.__words[id]

    def getId(self, word):  # always catch LookupError case sensitive p.e. german
        wordLC = word
        return self.__words.index(wordLC)

    ## gets a randomword, which is not listed in the unusableIds
    def getRandom(self, unusableIds = []):
        returnable = random.randint(0,(len(self.__words)-1))
        if not returnable in unusableIds: #returnable is alright, return
            self.__randTries=0
            return returnable
        elif ((len(self.__words) <= len(unusableIds)) or (self.__randTries>100)):
            # too many tries or not enough words, raise an error
            self.__randTries=0
            raise ValueError('Could not get a random key. Either there are to many keys not allowed to use or it took to many tries \n try adding more words or reducing the number of unusable keys')
        else: #bad luck, try again
            self.__randTries+=1 #avoid an endless loop
            return self.getRandom()

    def setPensum(self, pensum, time):
        if time!= Config.PENSUM_KEY_DAY and time!= Config.PENSUM_KEY_WEEK :
            #unknown time chosen, always default to day
            raise NameError(time+' is not a known value. Pensum time set to default: daily')
            Config.PENSUM_TIME_SET = Config.PENSUM_KEY_DAY
        else:
            Config.PENSUM_TIME_SET = time
        Config.PENSUM = int(pensum)
        Config.PENSUM_SET_ON = datetime.date.today() # will be used to determine f time ran out
        self.writeConfig(0)

    def copySoundFiles(self, source):
        #used when reading in new words, copy all the files from the folder chosen to the sounds folder
        dest = Config.LOCATIONS_BASE_PATH+Config.LOCATIONS_SOUND_PATH
        try:
            src_files = os.listdir(source)
            for file_name in src_files:
                full_file_name = os.path.join(source, file_name)
                if (os.path.isfile(full_file_name)): #check if it is a file
                    shutil.copy(full_file_name, dest) # if so copy it
        except OSError as e:
            print('Directory not copied. Error: %s' % e)
            #do not stop the program from wrking
            #proobably change on the to GUI warning



