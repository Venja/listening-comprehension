sourcecode
by Jana Huchtkoetter, 2016 

applause.ogg
Shortened, original Auditorium Applause Sound By Thore  [Public Domain], http://soundbible.com/1260-Auditorium-Applause.html

buzzer.ogg
von BlastOButter42 (Eigenes Werk) [Public domain], via Wikimedia Commons

Warnings 
created using DSpeech by Dimitrios Coutsoumbas (http://dimio.altervista.org/eng/)

Word audio files downloaded from forvo.com
converted to .ogg
pronunciations under a CC BY-NC-SA 3.0

pronunciations by camilorosa (http://forvo.com/user/camilorosa/)
	sol (http://forvo.com/word/sol/#es)
	gato (http://forvo.com/word/gato/#es)
	flor (http://forvo.com/word/flor/#es)

pronunciations by edwardyanquen (http://forvo.com/user/edwardyanquen/)
	mochila (http://forvo.com/word/mochila/#es)

pronunciations by DonQuijote (http://forvo.com/user/DonQuijote/) (profile not public)
	libro (http://forvo.com/word/libro/#es)
	casa (http://forvo.com/word/casa/#es)
	
pronunciations by Sento (http://forvo.com/user/Sento/)
	búho (http://forvo.com/word/búho/#es)
	
pronunciations by FelipeEsparzaC (http://forvo.com/user/FelipeEsparzaC/)
	árbol (http://forvo.com/word/árbol/#es)
	
pronunciations by arnaud (http://forvo.com/user/arnaud/)
	décembre (http://forvo.com/word/décembre/#fr)
	septembre (http://forvo.com/word/septembre/#fr)
	juillet (http://forvo.com/word/juillet/#fr)
	
pronunciations by gwen_bzh (http://forvo.com/user/gwen_bzh/)
	novembre (http://forvo.com/word/novembre/#fr)
	octobre (http://forvo.com/word/octobre/#fr)
	janvier (http://forvo.com/word/janvier/#fr)
	mai (http://forvo.com/word/mai/#fr)
	février (http://forvo.com/word/février/#fr)
	dimanche (http://forvo.com/word/dimanche/#fr)
	samedi (http://forvo.com/word/samedi/#fr)
	vendredi (http://forvo.com/word/vendredi/#fr)
	jeudi (http://forvo.com/word/jeudi/#fr)
	mercredi (http://forvo.com/word/mercredi/#fr)
	mardi (http://forvo.com/word/mardi/#fr)
	lundi (http://forvo.com/word/lundi/#fr)

pronunciations by NIKLA (http://forvo.com/user/NIKLA/)
	août (http://forvo.com/word/août/#fr)

pronunciations by muriel (http://forvo.com/user/muriel/)
	juin (http://forvo.com/word/juin/#fr)
	
pronunciations by ttiagob (http://forvo.com/user/ttiagob/)
	avril (http://forvo.com/word/avril/#fr)
	
pronunciations by margomichelsen (http://forvo.com/user/margomichelsen/)
	mars (http://forvo.com/word/mars/#fr)

