#Copyright Jana Huchtkoetter, 2016
#Creative Commons Attribution-ShareAlike 4.0 International License
#You are free to use, share and adapt this sourcecode
#It is designed to be executed on a Raspbian system, with a QR code scanner connected via USB, a button with input on pin 7 and a LED output on pin 5

#Please be aware that this affects ONLY the source code
#especially the files contained in the sound folder and in predefined and it subfolders are under CC BY-NC-SA 3.0
#further licenses are

#applause.ogg
#Shortened, original Auditorium Applause Sound By Thore  [Public Domain], http://soundbible.com/1260-Auditorium-Applause.html

#buzzer.ogg
#von BlastOButter42 (Eigenes Werk) [Public domain], via Wikimedia Commons

#Warnings
#created using DSpeech by Dimitrios Coutsoumbas (http://dimio.altervista.org/eng/)

#the files contained in the sound folder and in predefined and it subfolders are under CC BY-NC-SA 3.0
#please see the LICENSE.txt for a detailed list of all audio files

from GUI import *
from dataManager import *
from tkinter import Tk
from subprocess import Popen, PIPE, call
import sys
import serial
import RPi.GPIO as GPIO
#from signal import SIGTERM
import random
from mutagen.oggvorbis import OggVorbis
import threading
from datetime import date, timedelta
import time

#Game Types:
    #gametype 1
        #start with scanning a QR code
        #(try to) find the word with the dataManager
        #get some random words
        #pu them together in a random order
        #replay this set until button pressed or maximal 3 times
        #check if wrong or right
    #gametype 2
        #start by waiting for a button push
        #on button push find some random word
        #replay it at maximal 12 times, or until a card is scanned
        #check if correct card or wrong/maximum replay reached


class Gamecontroller(object):
    #now in the Config
    #BASE_PATH = '/home/pi/Documents/HuchtkoetterProjekt/ListeningComprehension/'
    #SOUND_PATH = 'sounds/'
    #BACKOFF = 700


    def __init__(self):
        self.thread = None
        random.seed()
        # HW init
        print("Startup")
        #call to a shell, very important as audio output isnt enabledby defalt
        call('amixer cset numid=3 1', shell=True)
        print("Audio configured")
        #configure the scanner on a serial, scanner must be configured to the same baudrate
        self.__port = '/dev/ttyUSB0'
        self.__baudrate = 9600
        self.ser = serial.Serial()
        self.ser = serial.Serial(port = self.__port, baudrate=self.__baudrate, timeout = 2)

        #configure the pins used by the button (output is the LED)
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(7, GPIO.IN)
        GPIO.setup(5, GPIO.OUT)

        self.__subproc = None
        self.__dataManager = DataManager(Config.LOCATIONS_BASE_PATH)
        self.__afters={} #important to cancel running threads in tk loop
        #print(self.__dataManager.getAudioFile(0))
        #game init
        #self.__roundId = -1
        self.__wordId = -1
        self.__gamingMode = False
        self.gameType = 1 #default
        self.__inRound = False
        self.__replayDict = {}
        self.__replayCount = 0
        self.__game2Scanned=""
        self.__game2Word=""
        self.__game2WordId=-1
        #UI init, create the root & gameframe
        self.__root = Tk()
        self.__root.geometry("550x350+300+300")
        self.__gameFrame = GameplayFrame(self.__root, self)
        #set the pensum to the one loaded from config
        self.__gameFrame.progress['value']=Config.PENSUM_FINISHED
        self.__afters[Config.AFTER_SCAN]=self.__root.after(1, self.scanWord)
        self.__root.mainloop()


    def endProg(self):#independent from game type
    #important to properly close processes etc
        self.stopGame()
        self.ser.close()
        #safe the configuration at the moment (pensum progress, pensum settings)
        self.__dataManager.writeConfig(self.__gameFrame.progress['value'])
        if (self.__subproc!=None and self.__subproc.poll()==None):
              self.__subproc.kill() #kill the process if running
        GPIO.cleanup()
        for val in self.__afters.items():
            self.__root.after_cancel(val) #kill all after processes
        print("Destroying window")
        self.__root.destroy()

#--------------------------- Polling externals --------------------------------------------------

    def scanWord(self):
        self.__root.update() #draw GUI bc. methods in loop hinder drawing
        if (self.__gamingMode):
            if(self.ser.isOpen() == False): #the ser has a timeout and can clos automatically
                self.ser.open()
            barcodeText = ""
            barcode = self.ser.read(100) #bytes to be read at max
            barcodeText = barcode.decode('utf-8') #must also be set on scanner
            barcodeText = barcodeText.rstrip()
            if (barcodeText != ""):
                self.__processScan(barcodeText) #defines what happens with the barcode
            else:  #scan again in 10 ms
                self.__afters[Config.AFTER_SCAN]=self.__root.after(10, self.scanWord)
       #else: no game ongoing, do not reschedule scan, will be done when game is finished / gamingMode starts

    def pollButton(self):
        if self.__gamingMode:
            if self.gameType==1 and self.__inRound: #ignore button click in gameTyoe1 when not in the middle of playing
                if(self.thread and self.thread.buttonPressed): #shortcircuit and
                    self.stopRound(False) #no error occured
                else: #check again on the started thread in 100 ms so the GUI stays responsive
                    #afters contains the id to all after processes so they can be killed
                    self.__afters[Config.AFTER_BUTTON]=self.__root.after(100, self.pollButton)
            else:
                if(self.thread and self.thread.buttonPressed):
                    self.thread.stop()
                    self.__startRound("") #needs an word argument, but mode2 takes random word so empty
                else:
                    self.__afters[Config.AFTER_BUTTON]=self.__root.after(100, self.pollButton)

#--------------------------- Game Settings & Control -------------------------------------------

    def changeGameType(self):
        print('Change Game Type')
        self.stopGame()
        if self.gameType==1:
            self.gameType=2
        else:
            self.gameType=1
        self.startGame()
        self.__gameFrame.startGame()

#GUIButton can alsways be used like the hardware button
    def GUIButtonInput(self):
        if(self.thread): #stop thread if still running
            self.thread.stop()
        if self.gameType==1:
            self.stopRound(False) #no error occured
        else:
            self.__startRound("")

#starts te game, in gameType 1 starts scanning the button
#in gameType 2 starts a new thread that polls the button and an after script
#which checks if there was a presure way less frequent
    def startGame(self):
        self.__gamingMode = True #now in game
        if (self.gameType == 1):
            self.__afters[Config.AFTER_SCAN]=self.__root.after(10, self.scanWord)
        else:
            self.__afters[Config.AFTER_BUTTON]=self.__root.after(10,self.pollButton)
            self.thread = pollThread()
            self.thread.start();

    def stopGame(self):
        self.__gamingMode=False #not in game anymore
        self.__inRound=False #not in a round anymore, important so no new after is scheduled
        if (self.thread):
            self.thread.stop()
            self.thread=None

#---------------------------- Game Flow -----------------------------------------------------
#anything regarding the real playing rounds

    def __startRound(self, word):
        if self.gameType==1:
            correctId=0 #safes the ID of the real correct word
            try:
                correctId = self.__dataManager.getId(word)
            except (LookupError,ValueError): #occurs when word could not be found
                self.__playInformation(Config.WARNING_UNKNOWN_WORD)
                self.__gameFrame.setWord("Couldn't find word "+word+",\n pls try again")
                self.__afters[Config.AFTER_SCAN]=self.__root.after(200, self.scanWord)
            else:
                #print(self.__roundId)
                self.__inRound = True #from now on playing a round
                self.__gameFrame.startRound(word)
                self.__checkPensum() #check if pensum must be reset
                #apply changes to frame before frequent loop tasks
                self.__root.update() #takes time!
                self.__findWords(correctId) #flow goes on here
        else: #gameType 2
            self.__inRound = True #from now on playing a round
            self.__checkPensum() #check if pensum must be reset
            self.__gameFrame.setWord("Waiting for \n scan")
            #grab 1 random word for this round
            self.__game2WordId = self.__dataManager.getRandom(self.__replayDict.values())
            self.__game2Word = self.__dataManager.getWord(self.__game2WordId)
            self.__playWord() #give out the word
            self.scanWord() #start scanning

    def stopRound(self, error):
        self.__inRound = False #round ended
        if (not error):
            if (self.gameType==1):
                if (self.thread): #end the thread
                    self.thread.stop()
                    self.thread=None
                if (self.__replayDict['nextPos']>0): #the word before nextPos was last replayed
                    lastPlayed = self.__replayDict[self.__replayDict['nextPos']-1]
                else: #if nextPos =0 the last word in array was replayed
                    lastPlayed =  self.__replayDict[3]
                #print(lastPlayed)
                #print(self.__dataManager.getAudioFile(lastPlayed))

                #give out the auditory feedback
                if (('correct' in self.__replayDict) and lastPlayed == self.__replayDict['correct']):
                    self.__playRightWrong(True)
                else:
                    self.__playRightWrong(False)
                Config.PENSUM_FINISHED=Config.PENSUM_FINISHED+1
                self.__dataManager.writeConfig(Config.PENSUM_FINISHED)
                self.__wordId = -1 #round finished, reset wordID to an unused one
                #start scanning again to be able to start next round
                self.__afters[Config.AFTER_SCAN]=self.__root.after(200, self.scanWord)
            else: #in gameType 2
                #give auditory feedback
                if self.__game2Scanned == self.__game2Word:
                    self.__playRightWrong(True)
                else:
                    self.__playRightWrong(False)
                self.__game2Scanned="" #reset the words
                self.__game2Word=""
                self.__game2WordId=-1 #unused
                self.__afters[Config.AFTER_BUTTON] = self.__root.after(10,self.pollButton)
                self.thread = pollThread() #start the thread & checking again to be able to start a new round
                self.thread.start();
        else:
            self.__gameFrame.stopRound(False) #do not add to pensum bar
            self.__gameFrame.setWord("There was \n an error")
        self.__replayCount = 0

#-------------------------- Helper Methods game flow -----------------------------------------------------
    def __checkPensum(self):
        #checks if the pensum time is reached & reset if so
        dateDiff = date.today() - Config.PENSUM_SET_ON
        if (Config.PENSUM_TIME_SET==Config.PENSUM_KEY_WEEK):
            if dateDiff.days >= 7:
                #print('reseting progress')
                self.__gameFrame.resetProgress();
                Config.PENSUM_SET_ON = date.today()
        else: #assume day
            if dateDiff.days >= 1:
                #print('reseting progress')
                Config.PENSUM_SET_ON = date.today()
                self.__gameFrame.resetProgress();

    def __playWord(self):
        #gameType 1 replays the 4 words repeatedly until the button is pressed
        if self.__inRound:  # gameType 1
            if (self.gameType == 1):
                if self.thread and self.thread.buttonPressed:
                    #print("PlayWord stopRound")
                    self.stopRound(False)  # False=no Error occured
                else:  # no button pressed, find word to be replayed
                    dictId = self.__replayDict['nextPos']
                    if dictId == 3:  # 4 entries: 0 .. 3
                        self.__replayDict['nextPos']=0
                    else:
                        self.__replayDict['nextPos']=dictId+1
                    playId = self.__replayDict[dictId]
                    self.__audioOutputWord(playId)  # flow continues here
            else:
                if len(self.__game2Scanned)<1: #replay if nothing has been scanned
                    self.__audioOutputWord(self.__game2WordId) #flow here

    def __audioOutputWord(self,playId):
        file_path = Config.LOCATIONS_BASE_PATH + Config.LOCATIONS_SOUND_PATH + self.__dataManager.getAudioFile(playId)
        #print(file_path)

        trackLength = OggVorbis(file_path).info.length #in seconds
        trackLength =int(1000*trackLength)  #int ms
        #increase replay Count, will onyl be replayed 12 times, 3 time all words in gameType 1 uand 12 rtimes same word in 2
        self.__replayCount+=1

        if(self.__replayCount<=12): #maximum 12 replays
            #calls ogg123 as external program
            self.__subproc = Popen('ogg123 '+file_path, shell=True, stdout=PIPE, stderr=PIPE)
            self.__afters[Config.AFTER_PLAY]= self.__root.after(trackLength+Config.GAME_BACKOFF, self.__playWord)
        else: #replayed to often, handle round as lost
            self.__replayDict['correct']=-1
            print("PlayWord stopRound timeout")
            self.stopRound(False) #False as no error occured

#gives out an accoustical right/wrong feedback
    def __playRightWrong(self, correct):
        if correct:
            self.__gameFrame.stopRound(True) #on game Frame, answer was correct
            command ='ogg123 ' + Config.LOCATIONS_BASE_PATH + 'applause.ogg'
        else:
            self.__gameFrame.stopRound(False) #on game Frame, answer was correct
            command ='ogg123 ' + Config.LOCATIONS_BASE_PATH + 'buzzer.ogg'
        ## Popen opens pipe os music can be played asynchronously
        #print(command)
        self.__subproc = Popen(command, shell=True, stdout=PIPE, stderr=PIPE)

#plays an informational message (speech to text)
    def __playInformation(self, audioFile):
        command ='ogg123 ' + Config.LOCATIONS_BASE_PATH + audioFile
        self.__subproc = Popen(command, shell=True, stdout=PIPE, stderr=PIPE)

#process the read in from scanner, handles special key words that can be scanned
#p.e. change of the game type
#normal scan starts a round in gameType 1 and stops it in gameTyoe 2
    def __processScan(self, barcode):
        print('Processing scan: '+barcode)
        if barcode==Config.KEYWORD_CHANGE_MODE:
            self.changeGameType()
        elif barcode == Config.PREDEFINED_FRENCH_KEY: #overwrite existing with predefined words
            csv = Config.PREDEFINED_FRENCH_CSV #only for legibility
            folder = Config.PREDEFINED_FRENCH_FOLDER
            self.updateConfig(folder,csv, True)
            print("Predefined french loaded")
            self.__afters[Config.AFTER_SCAN]=self.__root.after(10, self.scanWord)
        elif barcode == Config.PREDEFINED_SPANISH_KEY: #overwrite existing with predefined words
            csv = Config.PREDEFINED_SPANISH_CSV #only for legibility
            folder = Config.PREDEFINED_SPANISH_FOLDER
            self.updateConfig(folder,csv, True)
            print("Predefined spanish loaded")
            self.__afters[Config.AFTER_SCAN]=self.__root.after(10, self.scanWord)
        elif (self.gameType==1):
            self.__startRound(barcode)
        else:
            self.__game2Scanned = barcode
            self.stopRound(False) # no error occured


#-------------------------------- Methods with other classes (expecially data) --------------------------

#used in gameType1 to finde 3 additional random worda and create a random order
    def __findWords(self, correctId):#only game type 1
        try:
            #dictionary safes the correctid, stores words 0, 1, 2, 3, including the correct one
            #and nextPos which is used to circle over the words
            self.__replayDict={'correct':correctId}
            posCorrect = random.randint(0,3)
            self.__replayDict[posCorrect] = self.__replayDict['correct']
            #print("Korrekt an "+str(posCorrect))
            for i in range(0,4):
                if (i != posCorrect): #posCorrect already set
                    newId = self.__dataManager.getRandom(self.__replayDict.values())
                    self.__replayDict[i]=newId
    #        self.__wordId = 0
            self.__replayDict['counter']=0 #must be added AFTER for, as values otherwise would contain 0
            self.__replayDict['nextPos']=0
            self.thread = pollThread()
            self.__root.after(111, self.thread.start) #playWord first, game flow relies on the button
            self.__afters[Config.AFTER_PLAY]=self.__root.after(100,self.__playWord)
        except ValueError as err:
            #errors can occur when no random word sor not enough can ce found
            print(err)
            self.__playInformation(Config.WARNING_ERROR_RANDOM)
            self.stopRound(True) #there was an error

    def setPensum(self, nrWords, time, finished=0):
        #used to process the pensum set by a user in GUI and get it to data manager
        self.__dataManager.setPensum(nrWords,time)
        self.__gameFrame.progress['maximum']=nrWords
        self.__gameFrame.progress['value']=finished


    def updateConfig(self, soundFolder, csvFile, overwrite):
        #config in this case is the config of words and sounds, configuration itself is only written out on closing
        self.__dataManager.copySoundFiles(soundFolder)
        if overwrite:
            self.__dataManager.replaceCSV(csvFile)
        else:
            self.__dataManager.extendCSV(csvFile)




class pollThread ( threading.Thread ):

    def __init__(self):
        super(pollThread , self).__init__()
        self.stopped=False
        self.buttonPressed=False

    def run ( self ):
      while not self.stopped:
          if(GPIO.input(7) ==0):
                self.buttonPressed=True
                self.stopped=True
      print("Bye Bye")

    def stop(self):
        self.stopped=True