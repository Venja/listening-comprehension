# README #
Project written in Pxthon 3.x on a raspberry pi running raspbian

### What is this repository for? ###

This sourcecode was created to be run on a raspberry pi, conected to a button and a scanner capable of reading QR codes. 
It is meant to be used as a listening comprehension training gadget with two possible types of training.
First and the default is that somebody has QR codes printed out on flash cards. The QR code's contents must match some words read in from a .csv file and linked to a pronunciation sound file.
When the user scans a code the system choses the file connected to the card and 3 random files and replays them a maximum of 3 times each. If the user hits the buzzer in this time, the system checks if it was after the connected file. If so the pensum is updated. Either way the user gets an auditory feedback informing him if it was the right or wrong choice.

The second type starts when the user pushes the buzzer. The system choses one word at random and replays it a maximum of 12 times. The user must find the connected card and scan it in time. 
Like in the other type if the user was right the pensum is updated and he gets auditory feedback either way.

Version 1.0

### HARDWARE ###

If you want to implement this project, you will need some hardware as it is an embedded system (informations given re what I connected)

* a raspberry pi running raspbian (at least that#s where the code was written)
* a scanner connected via USB as a serial device, able to scan QR codes
* a button which can be pushed (like a big buzzer)

### How do I get set up? ###
If everything is assembled and Python 3.x is installed you still need the following library

* mutagen
* pySerial
* Rpi.GPIO should be installed on Raspbian

The files testbutton.py and barcode.py can help to test if the button & barcode scanner worked.
Furthermore you need to change paths in the Config according to the location on your system
It is also recommend to start the main.py on startup if it should be used as a stand-alone gadget without monitor or further inut devices

# Final remarks #
This project is at the moment not meant to be updated here or developed further, but it is a running gadget
If somebody is interested in recreating the same or a similiar gadget, message me and I can provide further information and some pictures