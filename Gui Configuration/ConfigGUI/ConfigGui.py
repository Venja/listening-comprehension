#Copyright Jana Huchtkoetter, 2016
#You are free to use, share and adapt this sourcecode
#Creative Commons Attribution-ShareAlike 4.0 International License
#Please be aware that this affects ONLY the source code
#especially the files contained in the sound folder and in predefined and it subfolders are under CC BY-NC-SA 3.0
#further licenses are

#applause.ogg
#Shortened, original Auditorium Applause Sound By Thore  [Public Domain], http://soundbible.com/1260-Auditorium-Applause.html

#buzzer.ogg
#von BlastOButter42 (Eigenes Werk) [Public domain], via Wikimedia Commons

#Warnings
#created using DSpeech by Dimitrios Coutsoumbas (http://dimio.altervista.org/eng/)

#the files contained in the sound folder and in predefined and it subfolders are under CC BY-NC-SA 3.0
#please see the LICENSE.txt for a detailed list of all audio files

from tkinter import *
from tkinter.ttk import *
from tkinter.filedialog import askopenfilename
from shutil import copyfile
import csv
import os



def getFile():
    filename = askopenfilename()
    eFile.delete(0, END)
    eFile.insert(0, filename)

def addWord():
    if eWord.get()!="" and eFile.get()!="":
        files[eWord.get()]=os.path.basename(eFile.get()) #extracts only the file name
        paths[eWord.get()]=eFile.get()
        listbox.insert(END, eWord.get())

def writeCSV():
#must! match the csv in the main program!
    with open('exportedWords.csv', 'w', newline='', encoding='utf-8') as csvfile: 
        writer = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
        writer.writerows(files.items())

def on_closing():
    root.destroy()

def safeAndClose():
    if len(files)>0:
        try:
            writeCSV()
            try:
                os.makedirs('sounds')
            except OSError as exception:
                if exception.errno != errno.EEXIST:
                    raise
            for key in paths:
                copyfile(paths[key], 'sounds/'+os.path.basename(paths[key]))
        except Exception as e:
            print(e.__doc__)
            print(format(e))
    on_closing()

def deleteSelected():
    # Delete from Listbox
    selection = listbox.curselection()
    # Delete from list that provided it
    print(selection[0])
    value = listbox.get(selection[0])
    print(value)
    listbox.delete(selection[0])
    del(files[value])
    del(paths[value])
    print(files)



root = Tk()

files = {}
paths = {}

#create the GUI elements, guzi is parted in a rifght fRight an left fLeft part 
#right contains list to scroll & buttons to delete
fRight = Frame(root)
btDelete = Button(fRight, text="Delete Selected", state=NORMAL, command=deleteSelected)
btDelete.grid(column=0, row=1)
btSafe = Button(fRight, text="Save and Close", state=NORMAL, command=safeAndClose)
btSafe.grid(column=1, row=1)
scrollbar = Scrollbar(fRight)
scrollbar.grid(column=2, row=0)

listbox = Listbox(fRight)
listbox.grid(column=0, row=0, columnspan=2)

# attach listbox to scrollbar
listbox.config(yscrollcommand=scrollbar.set)
scrollbar.config(command=listbox.yview)



#left contains inputs for files and names
fLeft = Frame(root)
Label(fLeft, text='Enter the word', borderwidth=1).grid(row=0, column=0)
eWord = Entry(fLeft)
eWord.grid(row=1, column=0)
Label(fLeft, text='Enter the audio file location', borderwidth=1).grid(row=2, column=0)
eFile = Entry(fLeft)
eFile.grid(row=3, column=0)
btSearch = Button(fLeft, text="Search", state=NORMAL, command=getFile)
btSearch.grid(row=3, column=1)
btAdd = Button(fLeft, text="Add word", state=NORMAL, command=addWord)
btAdd.grid(row=4,column=0)

fLeft.grid(column=0)
fRight.grid(column=1, row=0)

#call on_closing8) on delete
root.protocol("WM_DELETE_WINDOW", on_closing)
root.mainloop() #startup the GUI


